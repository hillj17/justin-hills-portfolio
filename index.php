<?php
/**
 * Created by PhpStorm.
 * User: hillj17
 * Date: 11/17/16
 * Time: 6:59 PM
 */
?>

<!DOCTYPE html>
<html>
<head>
    <title>Justin Hill's Portfolio</title>
    <link rel="icon" type="image" href="images/title_icon.png"/>
    <meta name="description" content="Online portfolio for Justin Craven Hill, a Software Developer and UI/UX Designer">
    <meta name="keywords" content="Justin Hill Software Developer UI UX Designer Northern Kentucky University">

    <link rel="stylesheet" type="text/css" href="css/main.css"/>
    <link rel="stylesheet" type="text/css" href="css/fullpage.min.css"/>

    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/fullpage.min.js"></script>
    <script>
        // See https://github.com/facebook/react/issues/20829#issuecomment-802088260
        if (!crossOriginIsolated) SharedArrayBuffer = ArrayBuffer;
    </script>
</head>
<body>
<nav class="menu-container loaded">
    <div id="menu">
        <ul id="main-menu">
            <!--<li data-menuanchor="about" class="active"><a href="#anchor-about">About</a></li>!-->
            <li data-menuanchor="portfolio"><a href="#portfolio">Portfolio</a></li>
            <li data-menuanchor="resume"><a href="#resume">Resume</a></li>
            <li data-menuanchor="contact"><a href="#contact">Contact</a></li>
        </ul>
    </div>
    <div class="toggle"><span></span><span></span><span></span></div>
</nav>

<div id="chartdiv"></div>

<div id="fullpage">

    <div class="section active" id="anchor-intro">
        <img id="fox" src="images/images_fox.png" height="500">
        <h1>Justin Hill</h1>
        <h2>Software Developer &amp; UI/UX Designer</h2>
    </div>
    <!--/<div class="section" id="anchor-about">
        <h1>fullPage.js</h1>
        <h2>Create Beautiful Fullscreen Scrolling Websites</h2>
        <div id="download"><a href="https://github.com/alvarotrigo/fullpage.js/archive/master.zip">Download</a></div><br />
        <p id="donations">
            <a href="extensions/">fullpage.js Extensions</a>
        </p>

        <div id="usedBy"></div>

        <div id="supportedBy">
            <div>Supported by:</div>
            <a href="http://www.browserstack.com/"><img src="http://wallpapers-for-ipad.com/fullpage/imgs3/logos/browserstack2.png" /></a>
        </div>

    </div>!-->
    <div class="section" id="anchor-portfolio">
        <div class="slide active">
            <div class="thriving-ripple">
                <div class="portfolio-title"><h2>Thriving Ripple</h2>Thriving Traditions, Inc.</div>
                <div class="portfolio-body"><p>Thriving Ripple is a website aimed at changing and shaping future generations
                        by helping individuals map out their living legacy.  The web app is designed to help users easily tell their story,
                        realize their ripple effect, and create traditions and rituals that matter the most.<br>
                        <i>Freelance Work | Lead Developer | Design | Graphics</i></p>
                </div>
                <div class="portfolio-link">
                    <a class="button" target="_blank" href="https://thrivingripple.com/">View Website
                        <span position="top"></span>
                        <span position="right"></span>
                        <span position="bottom"></span>
                        <span position="left"></span>
                    </a>
                </div>
            </div>
        </div>
        <div class="slide">
            <div class="street-reach">
                <div class="portfolio-title"><h2>Street Reach</h2>Center for Applied Informatics</div>
                <div class="portfolio-body"><p>First version of a mobile application aimed at benefiting the homeless.
                        The app allowed for homeless people to share their location to allow them easier access to receive outreach,
                        and allowed others to share the location of homeless people they found to provide the same benefits.
                        It also provided resources and directions to shelters for the homeless users.<br>
                        <i>Client-Requested Application | Android and iOS Programming | Design | Graphics</i></p>
                </div>
                <div class="portfolio-link">
                    <a class="button" target="_blank" href="https://www.facebook.com/streetreachcincy">View Facebook Page
                        <span position="top"></span>
                        <span position="right"></span>
                        <span position="bottom"></span>
                        <span position="left"></span>
                    </a>
                </div>
            </div>
        </div>
        <div class="slide">
            <div class="zerobus">
                <div class="portfolio-title"><h2>TARC ZeroBus</h2>Center for Applied Informatics</div>
                <div class="portfolio-body"><p>Mobile application for Louisville's zero emission, zero cost bus system.  App logs locations
                        of buses in real-time using Google Transit data and Google Maps.  Map displays numerous
                        attractions along and near bus routes and stops, and information about each attraction.<br>
                        <i>Client-Requested Application | Lead Developer | Android Programming | Design | Graphics</i></p>
                </div>
                <div class="portfolio-link">
                    <a class="button" target="_blank" href="https://brokensidewalk.com/2016/zerobus-app/">View Article
                        <span position="top"></span>
                        <span position="right"></span>
                        <span position="bottom"></span>
                        <span position="left"></span>
                    </a>
                </div>
            </div>
        </div>
        <div class="slide">
            <div class="super-swipe">
                <div class="portfolio-title"><h2>Super Swipe</h2>Personal Project</div>
                <div class="portfolio-body"><p>Android mobile game in which the user must swipe in
                        the direction of an arrow shown on screen.  Objective is to get as many
                        correct swipes as possible before time runs out.<br>
                        <i>Original game concept | Programming | Design | Graphics</i></p>
                </div>
                <div class="portfolio-link">
                    <a class="button" target="_blank" href="https://bitbucket.org/hillj17/superswipeandroid/src">View Repository
                        <span position="top"></span>
                        <span position="right"></span>
                        <span position="bottom"></span>
                        <span position="left"></span>
                    </a>
                </div>
            </div>
        </div>
        <div class="slide">
            <div class="bubble-assassin">
                <div class="portfolio-title"><h2>Bubble Assassin</h2>Personal Project</div>
                <div class="portfolio-body"><p>Android mobile game in which the user has to pop bubbles
                        which appear on screen and grow at an increasing rate, losing a point for
                        each bubble they miss until they run out of points.<br>
                        <i>Original game concept | Programming | Design | Graphics</i></p>
                </div>
                <div class="portfolio-link">
                    <a class="button" target="_blank" href="https://bitbucket.org/hillj17/bubble-assassin/src">View Repository
                        <span position="top"></span>
                        <span position="right"></span>
                        <span position="bottom"></span>
                        <span position="left"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="anchor-resume">
        <div class="slide active">
            <div class="experience">
                <h2>Experience</h2>
                <div class="icon"></div>
                <div class="text">
                    <p>I've gained an array of knowledge from working professionally with three drastically different companies.</p>
                    <ul>
                        <li><b>Car-Part.com (Software Developer)</b> - One of three developers that design and develop new and existing features
                            on our website and other software platforms.  Go-to person for Java client installer development,
                            Java client support software, HTML/CSS/JavaScript design questions, UI/UX design, SEO using SemRush and Google Analytics,
                            QA Automation C++ Development in Ranorex, among other things.
                        </li>
                        <li><b>Thriving Traditions, Inc. (Freelance Developer/Designer)</b> - Led design and development effort for new startup,
                            creating an online service which stores the user's memories and traditions in order to share this info with the user's families,
                            managing the integration of QuickBooks payment processing services, and developing secure solutions for storing user's sensitive information.
                        </li>
                        <li><b>Convergys (Programmer Intern)</b> - Added web development experience to my skill set working on a
                            corporation's website and CMS, worked on a small team using
                            BitBucket to modify one code base and deliver requirements each release
                        </li>
                        <li><b>Center for Applied Informatics (Mobile Applications Developer)</b> - Learned aspects of programming for
                            Android-based mobile applications, worked in teams to complete large-scale projects for big-name companies
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="slide">
            <div class="education">
                <h2>Education</h2>
                <div class="icon"></div>
                <div class="text">
                    <p>I've extremely proud of the education I've received from both college and high school.</p>
                    <ul>
                        <li><b>Northern Kentucky University</b> - 3.673 GPA <br>
                            Bachelor of Science in Computer Science, Bachelor of Arts in Media Informatics<br>
                            University Honors Scholar
                        </li>
                        <li><b>Bracken County High School</b> - 4.27 GPA <br>
                            Graduated Top Male in my class (#4 overall)
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="slide">
            <div class="programming-skills">
                <h2>Programming Skills</h2>
                <div class="icon"></div>
                <div class="text nobullets">
                    <p>I've worked towards becoming a full-stack developer first, and a designer second.  Here's my programming skill set:</p>
                    <div id="skillsList">
                        <ul>
                            <li>Java</li>
                            <li>Tomcat</li>
                            <li>PHP</li>
                            <li>JavaScript</li>
                            <li>HTML</li>
                            <li>CSS</li>
                            <li>Ajax</li>
                        </ul>
                        <ul>
                            <li>jQuery</li>
                            <li>jQueryUI</li>
                            <li>Delphi/Pascal</li>
                            <li>Android</li>
                            <li>iOS</li>
                            <li>C</li>
                            <li>C++</li>
                        </ul>
                        <ul>
                            <li>NSIS</li>
                            <li>WinBatch</li>
                            <li>Python</li>
                            <li>Oracle</li>
                            <li>SQL</li>
                            <li>MySQL</li>
                            <li>SQLite</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide">
            <div class="design-skills">
                <h2>Design Skills</h2>
                <div class="icon"></div>
                <div class="text">
                    <p>I wouldn't consider myself an artist, but I do know how to work with graphic design tools to
                        create mock-ups for projects or features I'm designing.  I'm also fairly skilled in video editing.</p>
                    <ul>
                        <li>UI/UX</li>
                        <li>Illustrator</li>
                        <li>Photoshop</li>
                        <li>Video Editing</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="section" id="anchor-contact">
        <div class="contact">
            <h1>Contact Me</h1>
            <h2>Email:  justin.bobbyhill7@gmail.com</h2>
            <h2>Phone:  (606) 782-5666</h2>
            <form id="contact_form" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <label for="name">Your name:</label><br />
                    <input id="name" class="input" name="name" type="text" value="" size="30" required /><br />
                </div>
                <div class="row">
                    <label for="email">Your email:</label><br />
                    <input id="email" class="input" name="email" type="text" value="" size="30" required/><br />
                </div>
                <div class="row">
                    <label for="message">Your message:</label><br />
                    <textarea id="message" class="input" name="message" rows="7" cols="30" required></textarea><br />
                </div>
                <button id="emailButton" class="submit">Send Email
                    <span position="top"></span>
                    <span position="right"></span>
                    <span position="bottom"></span>
                    <span position="left"></span>
                </button>
                <div id="sent" hidden>
                    Your email was sent successfully, I will reply to you soon!
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#fullpage').fullpage({
            licenseKey: 'A72F006C-DA5D46A2-B78F8203-279FFEDD',
            //sectionsColor: ['#ffffff', '#1bbc9b', '#4BBFC3', '#7BAABE', 'whitesmoke'],
            //anchors: ['intro', 'about', 'portfolio', 'resume', 'contact'],
            sectionsColor: ['#ffffff', '#050b15', '#98a6af', '#7a3e05'],
            anchors: ['intro', 'portfolio', 'resume', 'contact'],
            menu: '#menu',
            css3: true,
            verticalCentered: true,
            scrollingSpeed: 1000
        });

        $('#contact_form').submit(function (e) {

            e.stopImmediatePropagation();
            e.preventDefault();

            $.ajax({
                url: 'php/mail.php',
                type: 'POST',
                data: $('#contact_form').serialize(),
                success: function (data) {
                    var sent = $("#sent");
                    if (data != "") {
                        sent.text("Something went wrong, your email was not sent successfully.");
                    }
                    $("#emailButton").hide();
                    sent.fadeIn();
                }
            })
        });
    });
</script>

</body>
</html>
